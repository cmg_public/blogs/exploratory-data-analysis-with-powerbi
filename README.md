# Exploratory Data Analysis with PowerBI

## Overview

This repository has one PowerBI file (PowerBI_EDA.pbix) that outlines how to conduct some basic exploratory data analysis on the CSV file contained in this repository (world_population.csv).

There is a blog article that goes along with this repository, which can be found [here](https://www.515tech.com/blog).
